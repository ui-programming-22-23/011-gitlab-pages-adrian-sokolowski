let sizeChange = false;
let pacManMoving = false;
let mouthOpen = true;
let appleEaten = false;
let mouthReOpen = true;
let scoreAmount = 0;
let score = document.getElementById("scoreDiv");

let movingLeft = false;
let movingRight = false;
let movingUp = false;
let movingDown = false;
let yellowButton = document.getElementsByClassName("yellow")[0];
let blueButton = document.getElementsByClassName("blue")[0];
let redButton = document.getElementsByClassName("red")[0];
let greenButton = document.getElementsByClassName("green")[0];
let magentaButton = document.getElementsByClassName("magenta")[0];

let animationTimer = 0;

function getRandomIntegre( max, min)
{
    return Math.floor(Math.random()*(max - min));
}
function VectorObject(x,y)
{
    this.x = x;
    this.y = y;
}
function GameObject(spritesheet, x, y, width, height, dX,dY,dW,dH) 
{
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.dX = dX;
    this.dY = dY;
    this.dW = dW;
    this.dH = dH;
    
    return this;
}
function KeyInput(input)
{
    this.action = input;
    return this;
}
let appleX = getRandomIntegre(canvas.width, 40);
let appleY = getRandomIntegre(canvas.height, 40) ;
console.log(appleX,appleY);

let mousePos = new VectorObject(0,0);
let userInput = new KeyInput("None");
let player = new GameObject(pacmanSpritesheet,8,7,pacmanWidth,pacmanHeight,200,200,200,200);
let apple = new GameObject(collectable,appleX,appleY,appleWidth,appleHeight);
function input(event) 
{
    // Take Input from the Player
    console.log("Input");
    console.log("Event type: " + event.type);
    console.log("Keycode: " + event.keyCode);
    if (event.type === "keydown")
        {
            switch (event.keyCode) 
            {
                case 65: // A Key
                blueButton.classList.add("pressed");
                movingLeft = false;
                movingRight = false;
                movingUp = false;
                movingDown = false;
                  //  userInput = new KeyInput("A")
                break; 
                case 87: // W Key
                yellowButton.classList.add("pressed");
                movingLeft = false;
                movingRight = false;
                movingUp = false;
                movingDown = false;
                  //  userInput = new KeyInput("W")
                break;
                case 68: // D Key
                redButton.classList.add("pressed");
                movingLeft = false;
                movingRight = false;
                movingUp = false;
                movingDown = false; 
                  //  userInput = new KeyInput("D")
                break; 
                case 83: // S Key
                greenButton.classList.add("pressed");
                movingLeft = false;
                movingRight = false;
                movingUp = false;
                movingDown = false;
                   // userInput = new KeyInput("S")
                break; 
                case 67: // C Key
                
                 userInput = new KeyInput("C");
                break;
                default:
                   // userInput = new KeyInput("None");
                    break;
            }
        }
        else {
            
            redButton.classList.remove("pressed");
            blueButton.classList.remove("pressed");
            yellowButton.classList.remove("pressed");
            greenButton.classList.remove("pressed");
    
        }
    if (event.type === "mousedown")
    {
        console.log("moise")
        mousePos = getMousePos(canvas, event)
        console.log(mousePos.x,mousePos.y)
    }
} 
function moveLeft()
{
    player.dX = player.dX - 5;
}
function moveUp()
{
    player.dY = player.dY - 5;
}
function moveRight()
{
    player.dX = player.dX + 5;
}
function moveDown()
{
    player.dY = player.dY + 5;    
}
function gameLoop()
{
    update();
    draw();
    
    score.innerHTML = "Score : " + scoreAmount;
    window.requestAnimationFrame(gameLoop,(1000 % 60));
}
function update()
{
    if(blueButton.classList.contains("pressed") || movingLeft)
    {
        moveLeft();
        if (player.y != 7)
        {
            player.y = 7;
        }
        if(animationTimer == 15)
        {
            player.x += 20;
        }
        else if (animationTimer == 30)
        {
            player.x += 20
        }
        else if (animationTimer == 35)
        {
            player.x -= 40
            animationTimer = 0
        }
        animationTimer += 1;
        
       // console.log("A pressed");
    }
    if(yellowButton.classList.contains("pressed") || movingUp )
    {
        moveUp();
        if (player.y != 47)
        {
            player.y = 47;
        }
        if(animationTimer == 15)
        {
            player.x += 20;
        }
        else if (animationTimer == 30)
        {
            player.y = 7;
            player.x += 20
        }
        else if (animationTimer == 35)
        {
            
            player.x -= 40
            animationTimer = 0
        }
        animationTimer += 1;
        
       // console.log("W pressed");
    }
    if(redButton.classList.contains("pressed") || movingRight )
    {
        moveRight();
        if (player.y != 27)
        {
            player.y = 27;
        }
        if(animationTimer == 15)
        {
            player.x += 20;
        }
        else if (animationTimer == 30)
        {
            player.y = 7;
            player.x += 20
        }
        else if (animationTimer == 35)
        {
            player.x -= 40
            animationTimer = 0
        }
        animationTimer += 1;
        
        
        
       // console.log("D pressed");
    }
    if(greenButton.classList.contains("pressed") || movingDown )
    {
        moveDown();
        if (player.y != 67)
        {
            player.y = 67;
        }
        if(animationTimer == 15)
        {
            player.x += 20;
        }
        else if (animationTimer == 30)
        {
            player.y = 7;
            player.x += 20
        }
        else if (animationTimer == 35)
        {
            player.x -= 40
            animationTimer = 0
        }
        animationTimer += 1;
        
        //console.log("S pressed");
    }
   
    if(appleEaten == true)
    {
        scoreAmount++;
        hpVal += 10;
        appleEaten = false;
        apple.x = getRandomIntegre(canvas.width,appleWidth);
        apple.y = getRandomIntegre(canvas.height, appleHeight);
    }
    wallCollision();
    appleCollison();
    if(mousePos.x >= 0 && mousePos.x <= 1200 && mousePos.y >= 0 && mousePos.y <= 200 )
    {
        //console.log("in")
        console.log(mousePos.x,mousePos.y)
        
    }
    
}
function draw()
{
    context.clearRect(0,0,canvas.width,canvas.height);
    context.drawImage
    (
        player.spritesheet,

        player.x,
        player.y,
        player.width,
        player.height,

        player.dX,
        player.dY,
        player.dW,
        player.dH,
    );
    context.drawImage
    (
        apple.spritesheet,
        apple.x,
        apple.y,
        apple.width,
        apple.height
    );
    
    drawHealthbar();
}

window.requestAnimationFrame(gameLoop,(1000 % 60));

window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
window.addEventListener('mousedown', input);
function clickableDpadReleased() {
    console.log(event);
}
function clickDpadYellow(){
    movingUp = true;
    movingLeft = false;
    movingRight = false;
    movingDown = false;
    console.log(event);

}
function clickDpadBlue(){
    movingLeft = true;
    movingRight = false;
    movingUp = false;
    movingDown = false;
    console.log(event);
}
function clickDpadRed(){
    movingRight = true;
    movingLeft = false;
    movingUp = false;
    movingDown = false;
    console.log(event);
}
function clickDpadGreen(){
    movingDown = true;
    movingLeft = false;
    movingRight = false;
    movingUp = false;
    console.log(event);
}

function getMousePos(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top
    };
  }


  var dynamic = nipplejs.create({
    
    color: 'grey',
    position: {
        x: 100,
        y: 200
    }
    
});

dynamic.on('added', function (evt, nipple) {
    //nipple.on('start move end dir plain', function (evt) {
    nipple.on('dir:up', function (evt, data) {
        movingUp = true;
        movingLeft = false;
        movingRight = false;
        movingDown = false;
    });
    nipple.on('dir:down', function (evt, data) {
        movingDown = true;
        movingLeft = false;
        movingRight = false;
        movingUp = false;
     });
     nipple.on('dir:left', function (evt, data) {
        movingLeft = true;
        movingRight = false;
        movingUp = false;
        movingDown = false;
     });
     nipple.on('dir:right', function (evt, data) {
        movingRight = true;
        movingLeft = false;
        movingUp = false;
        movingDown = false;
     });
     nipple.on('end', function (evt, data) {
        //console.log("mvmt stopped");
        gamerInput = new KeyInput("None");
     });
    
});
