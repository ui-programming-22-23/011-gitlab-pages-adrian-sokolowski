let pacManOpen = new Image;
pacManOpen.src = "assets/img/pac-man.png"

let pacManClosed = new Image;
pacManClosed.src = "assets/img/PacManClose.jpg"

let pacmanSpritesheet = new Image;
pacmanSpritesheet.src = "assets/img/pacman-retro.png"

let collectable = new Image;
collectable.src = "assets/img/apple.png"


let canvas = document.getElementById("drawArea");
let context = canvas.getContext("2d");

let appleWidth = 40; let appleHeight = 40;
let pacmanWidth = 12; let pacmanHeight = 13;
var hpVal = 10;

function drawHealthbar() {
  var hpWidth = 100;
  var hpHeight = 20;
  var hpMax = 100;

  // Draw the background
  context.fillStyle = "#000000";

  context.fillRect(0, 0, hpWidth, hpHeight);

  // Draw the fill
  context.fillStyle = "#00FF00";
  var fillVal = Math.min(Math.max(hpVal / hpMax, 0), 1);
  context.fillRect(0, 0, fillVal * hpWidth, hpHeight);
}
  